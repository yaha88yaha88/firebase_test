const functions = require('firebase-functions');
// The Firebase Admin SDK to access Cloud Firestore.
const admin = require('firebase-admin');

const moment = require('moment');

// cloud functionでfirestoreを使うのに必要な設定
admin.initializeApp(functions.config().firebase);
// データベースの参照を作成
var fireStore = admin.firestore();
// ニュースのスタブデータ保管先
var stbData = null;

// スタブデータをDBから取得する処理
function stbUpdate() {
  console.log('stbUpdate exec');
  const newsRef = fireStore.collection('bankNews').doc('bnknws');
  newsRef.get()
    .then(doc => {
    if (!doc.exists) {
      stbData = 'No such document!';
    } else {
      stbData = doc.data();
    }
    return;
  }).catch(() => {
    stbData = 'not found';
    return;
  });
}
// Cloud Functionsのデプロイ時に実行
stbUpdate();

// Firestoreのスタブデータが更新された際に実行(スタブデータの更新)
exports.StbUpdateTriger = functions.firestore
  .document('BankNews/bnknws')
  .onWrite((change, context) => {
  console.log('StbUpdateTriger exec');
  stbUpdate();
});

/** firestore利用, 認証無しで実行可能 */
exports.getBankNewsStb = functions.https.onRequest((request, response) => {
  console.log('getBankNewsStb exec');
  // データの送信
  response.send(stbData);
});

/** firestore、onCallで認証情報をチェックする */
exports.callBankNewsStb = functions.https.onCall((data, context) => {
  // 認証情報の有無をチェック
  if (context.auth) {
    // Firebaseへ書き込むデータの準備
    const date = moment().format('YYYYMMDD_hhmmss');
    var logData = {};
    // 日時をフィールド名として利用
    logData[date] = {
      auth: JSON.stringify(context.auth),
      method: 'callBankNewsStb',
      date: new Date(),
    };
    // Firestoreへの書き込み
    // ※事前にFirestoreにコレクション'BankNewsStbAccess'とそのドキュメント'access'を用意しておく必要がある。
    const accessRef = fireStore.collection('BankNewsStbAccess').doc('access');
    accessRef.update(logData);
    console.log('callBankNewsStb auth OK!');

    // データの送信
    return stbData;
  } else {
    console.error('callBankNewsStb error!');
    throw new functions.https.HttpsError('permission-denied', 'Auth Error');
  }
});

// トークンの取得
const registrationTokens = [
  'dvv6cYiyGQiwIOgriyLHr1:APA91bFAD00CaOrAqzXKIzlRxnChF48gAlANRTDvhTPetOEuqu4nbLEWOSDNCUroUZ8PmjBbp4YpHavWJZH0zBB2El8XRfs9Y3qvjJ57jgnk6AcHxD-zE1e0W9ejPQr0YzWEuGtjzS0a',
  'O7_TeRLnlktCVNnyr:APA91bHKgT5Pn7aAfp9nEzbNCTUtGmKsViwZSck99viMRsndHyp7z0l_KJ1aC_QkR932foJAaiHePD87HcLBiemkwNHeQEx33nJYzK0MG67MIYFWVEstKZ-tlLa4ou5eK1nGFU0bjdaQ'
];
// push確認用
exports.callFcm = functions.https.onCall((data, context) => {
  // 認証情報の有無をチェック
  if (context.auth) {
    // Firebaseへ書き込むデータの準備
    const date = moment().format('YYYYMMDD_hhmmss');
    var logData = {};
    // 日時をフィールド名として利用
    logData[date] = {
      auth: JSON.stringify(context.auth),
      method: 'callFcm',
      date: new Date(),
    };
    // Firestoreへの書き込み
    // ※事前にFirestoreにコレクション'BankNewsStbAccess'とそのドキュメント'access'を用意しておく必要がある。
    const accessRef = fireStore.collection('BankNewsStbAccess').doc('access');
    accessRef.update(logData);
    console.log('callBankNewsStb auth OK!');

    // Push https://firebase.google.com/docs/cloud-messaging?hl=ja
    const message = {
      notification: {
        title: 'push通知',
        body: 'データが更新されました'
      },
      data: {
        test: 'test123',
        message: 'message123'
      },
      tokens: registrationTokens,
    };
    admin.messaging().sendMulticast(message)
      .then((response) => {
        // Response is a message ID string.
        console.log('Successfully sent message:', response);
        return;
      })
      .catch((error) => {
        console.log('Error sending message:', error);
      });

    // データの送信
    return stbData;
  } else {
    console.error('callFcm error!');
    throw new functions.https.HttpsError('permission-denied', 'Auth Error');
  }
});
